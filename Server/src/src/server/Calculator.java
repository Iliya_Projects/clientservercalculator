package src.server;

import java.util.Vector;


public class Calculator {

	public int add(Vector<Integer> input){
		int result = 0;
		for(int i= 0;i < input.size(); i++){
			result += input.elementAt(i);
		}
		
		return result;
		
	}
}
