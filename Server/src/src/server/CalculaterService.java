/**
 * @author iliya
 * 
 * this will be the thread that will handle a single connection
 */
package src.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Vector;

import org.json.*;

public class CalculaterService extends Thread {
	private Socket client;
	private StringBuilder clientsMessage;
	private int result;
	private StringBuilder resultMssg;

	public CalculaterService(Socket clientSocket) {
		this.client = clientSocket;
		resultMssg = new StringBuilder();
	}

	@Override
	public void run() {
		BufferedReader fromClient;
		DataOutputStream toClient;
		Boolean connected = true;
		String line;
		clientsMessage = new StringBuilder();
		try {
			fromClient = new BufferedReader( // Datastream FROM Client
					new InputStreamReader(client.getInputStream()));
			toClient = new DataOutputStream(client.getOutputStream()); // Datastream
																		// TO
																		// Client
			while (connected) { // As long as connection exists
				line = fromClient.readLine();
				if (line.isEmpty()) {
					if (checkClientsMessage(new String(clientsMessage))) {
						readMessage(new String(clientsMessage));
						resultMssg.append("\n");
						String resulted = new String(resultMssg);
						toClient.writeBytes(resulted);
					} else {

						toClient.writeBytes("400 Error \n");

					}
					connected = false;
				} else {
					clientsMessage.append(line + "\n");
				}
			}

			fromClient.close();
			toClient.close();
			client.close();
			System.out.println("Session ended, Server remains active");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// for now check only if the request contains a few
	// standard informations
	private synchronized boolean checkClientsMessage(String clientMessage) {
		if (clientMessage.contains("POST") || clientMessage.contains("GET"))
			if (clientMessage.contains("Content-Length")
					&& clientMessage.contains("Content-Type"))
				return true;
		return false;
	}

	private synchronized void readMessage(String clientsMessage) {
		String[] splitedMessage = clientsMessage.split("\n");
		StringBuilder jsonBuilder = new StringBuilder();
		for (int i = 5; i < splitedMessage.length; i++) {
			jsonBuilder.append(splitedMessage[i]);
		}
		try {
			readJsonFile(jsonBuilder);
		} catch (Exception e) {
			resultMssg.append(e);
		}
	}

	private void readJsonFile(StringBuilder jsonBuilder) throws Exception {
		JSONObject clientMessage = new JSONObject(new String(jsonBuilder));
		String funciton = clientMessage.getString("function");
		JSONObject params = clientMessage.getJSONObject("params");
		Vector<Integer> parameters = new Vector<Integer>();
		String[] names = JSONObject.getNames(params);
		for (int i = 0; i < names.length; i++) {
			parameters.add(params.getInt(names[i]));
		}

		switch (funciton) {
		case "add":
			Calculator calc = new Calculator();
			result = calc.add(parameters);
			resultMssg.append("The result is: " + result + "\n");
			break;
		default:
			throw new Exception("No such method! \n");
		}

	}

}
