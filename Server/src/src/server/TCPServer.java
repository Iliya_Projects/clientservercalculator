/**
 * @author iliya
 *  This is the simple implementation of the TCP server, that will get a
 *  task from a client.
 */
package src.server;

import java.io.*;
import java.net.*;

public class TCPServer {
//	static String line;
//	static BufferedReader fromClient;
//	static DataOutputStream toClient;

	// To  do : remake it to a multithreaded server
	public static void main(String[] args) throws Exception {
		ServerSocket contactSocket = new ServerSocket(9997);
		while (true) { // Handle connection request
			Socket client = contactSocket.accept(); // creat communication
													// socket
			System.out.println("Connection with: "
					+ client.getRemoteSocketAddress());
			new CalculaterService(client).start();
			//handleRequests(client);
		}
	}
}
