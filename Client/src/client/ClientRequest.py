'''
Created on Feb 16, 2016

Simple client example

@author: iliya
'''
import socket
import httplib2

class ClientRequest:
    '''
    classdocs
    '''
    serverPort = 9997
    serverIp = "127.0.0.1"
    simpleMessage = "Hello Server"   
    bufferSize =  1024
    openSocket = 0

    def __init__(self,message):
        #self.serverIp = serverIP
        #self.serverPort = serverPort
        self.simpleMessage = message
        self.openSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        self.openConnection()
        self.sendMessage()
        self.recieveMessage()
        self.closeConnection()
    
    def openConnection(self):
        self.openSocket.connect((self.serverIp,self.serverPort))
        
    
    def sendMessage(self):
        self.openSocket.send(self.simpleMessage)
        
        
    def recieveMessage(self):
        recieved = ""
        bytesRecieved = 0
        while bytesRecieved < self.bufferSize:
            chunk = self.openSocket.recv(self.bufferSize)
            if chunk.endswith('\n'):
                #recieved.append(chunk)
                recieved += chunk
                break
            #recieved.append(chunk)
            recieved += chunk
            bytesRecieved = bytesRecieved + len(chunk)  
           
        print recieved
        
    def closeConnection(self):    
        self.openSocket.close()